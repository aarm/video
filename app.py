import pickle
from http import HTTPStatus
import random
import sys
import subprocess
import botocore
import uuid
import moviepy.editor as mp
import requests
from botocore.client import logger
from botocore.exceptions import ClientError
from flask_cors import CORS
from moviepy.editor import *
from moviepy.editor import VideoFileClip, AudioFileClip
import moviepy.editor as mpe
from flask import Flask, request, json, Response
import boto3
from moviepy.video.fx.crop import crop
from werkzeug.utils import secure_filename

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})


@app.route('/', methods=['GET'])
def welcome():
    return "Weolcome to Video Service"


@app.route('/edit', methods=['POST'])
def uploadedit():
    global videoClip, videoClipSize, imageSize, backAudio, audioMixing, audio, videoLength, id
    editData = request.get_json()  # ALL Data
    id = editData['id']
    video = f'https://media-storage-beta.s3.amazonaws.com/videos/{id}.mp4'
    searchVideo = boto3.client('s3',    #s3 setup to take object
                               aws_access_key_id='AKIAZ5H56UEOT3F33ZPM',
                               aws_secret_access_key='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',
                               region_name='ap-south-1'
                               )
    # object = searchVideo.get_object(Bucket='media-storage-beta', Key='videos/{}.mp4'.format(id))
    try:
        object = searchVideo.get_object(Bucket='media-storage-beta', Key='videos/{}.mp4'.format(id))    #check object that present or not
    except ClientError as ex:
        if ex.response['Error']['Code'] == 'NoSuchKey':
            logger.info('No object found - returning empty')
            return Response(json.dumps({"status": False, "message": "video not found"}),
                            status=HTTPStatus.BAD_REQUEST)
        else:
            raise
    try:
        audio = editData['config']['audio_file']
    except KeyError as e:
        pass
        # return Response(json.dumps({"status": False, "message": "Audio file not found"}), status=HTTPStatus.BAD_REQUEST)
    try:
        audioMixing = editData['config']['audio_mixing']
    except KeyError as e:
        return Response(json.dumps({"status": False, "message": "Audio mixing not found"}),
                        status=HTTPStatus.BAD_REQUEST)
    try:
        videoLength = editData['config']['length']
        print(videoLength, 'length')
    except KeyError as e:
        return Response(json.dumps({"status": False, "message": "video length not found"}),
                        status=HTTPStatus.BAD_REQUEST)

    if video and audioMixing is True:  # Genrate video with new audio and length
        videoClip = VideoFileClip(video).subclip(0, videoLength).resize(1)
        videoClipSize = videoClip.w
        backgroundAudio = mpe.AudioFileClip(audio).subclip(0, videoClip.duration)
        mergeAudio = videoClip.set_audio(backgroundAudio)
        imageSize = int(round((videoClipSize * 30) / 100))  # make watermark 30% width of video width
        setImage = ImageClip('video_water_mark.png').set_position(("left", "top")).set_duration(
            videoClip.duration).resize(width=imageSize)
        result = CompositeVideoClip([mergeAudio, setImage])
        result.write_videofile('{}.mp4'.format(id))
    else:  # genrate video without new audio
        videoClip = VideoFileClip(video).subclip(0, videoLength).resize(1)
        videoClipSize = videoClip.w
        backgroundAudio = mpe.AudioFileClip(video).subclip(0, videoClip.duration)
        mergeAudio = videoClip.set_audio(backgroundAudio)
        imageSize = int(round((videoClipSize * 30) / 100))  # make watermark 30% width of video width
        setImage = ImageClip('video_water_mark.png').set_position(("left", "top")).set_duration(
            videoLength).resize(width=imageSize)
        result = CompositeVideoClip([mergeAudio, setImage])
        result.write_videofile('{}.mp4'.format(id))

    file = '{}.mp4'.format(id)
    s3 = boto3.resource('s3',       #s3 setup for upload in new folder
                        aws_access_key_id='AKIAZ5H56UEOT3F33ZPM',
                        aws_secret_access_key='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',
                        )
    response = s3.meta.client.upload_file(
        file, 'my-take-vod-source-1rhnu2hd5t49r', file, ExtraArgs={'ContentType': 'video/mp4'})
    return Response(json.dumps({"status": True, "message": "Video edited successfully "}), status=HTTPStatus.ACCEPTED)


@app.route('/removeWaterMark', methods=['POST'])
def removeWaterMark():
    videoClip = mp.VideoFileClip('tiktok4.mp4')
    videoLength = videoClip.duration
    height = videoClip.h
    newHeigth = int(round((height * 80) / 100))
    finalVideo = videoClip.subclip(0, videoLength - 3)
    videoClip1 = crop(finalVideo, y1=80, height=newHeigth)
    videoClip1.write_videofile('finalVideo.mp4')

    return 'videoClip'


@app.route('/generateUrl', methods=['POST'])
def generateUrl():
    global signedUrl
    uniqueId = uuid.uuid4()  # generate unique id
    bucket = 'media-storage-beta'
    key = 'videos/{}.mp4'.format(uniqueId)
    s3 = boto3.client('s3',
                      aws_access_key_id='AKIAZ5H56UEOT3F33ZPM',
                      aws_secret_access_key='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',
                      region_name='ap-south-1'
                      )
    signedUrl = s3.generate_presigned_post(Bucket=bucket,  # generate presigned url
                                           Key=key,
                                           Fields={
                                               'acl': 'public-read',
                                           },
                                           Conditions=[
                                               {"acl": "public-read"},
                                           ]
                                           )
    # os.system('curl   --request PUT --upload-file tiktok.mp4 "' + signedUrl + '"')
    objectURL = {
        'url_data': signedUrl,
        'id': uniqueId,
        'extension': 'MP4'
    }

    return Response(json.dumps({"status": True, "message": "Presigned Url generated ", "data": objectURL}),
                    status=HTTPStatus.ACCEPTED)


if __name__ == '__main__':
    app.run(debug=True)
